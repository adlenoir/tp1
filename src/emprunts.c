#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include "emprunts.h"
#include "utils.h"
#define __DEBUG__ 1

/* -------------------------------------------------------------------- */
/* creerEmprunts          		Creer une liste vide 	                */
/*                                                                      */
/* Principe: Sert de base en cas de structures de liste plus complexes  */
/*                                                                      */
/* En entrée:								                            */
/*                                                                      */
/* En sortie: Un pointeur pointant vers NULL					        */
/* -------------------------------------------------------------------- */
liste_emprunts creerEmprunts()
{
    return NULL;
}

/* -------------------------------------------------------------------- */
/* creerEmprunt  Creer un emprunt contenant un numéro et une date       */
/*                                                                      */
/* Principe: Alloue une cellule et la remplie avec les vars fournies    */
/*                                                                      */
/* En entrée: num : numéro du livre emprunté                            */
/*			  date_retour : date à laquelle le livre doit être retourné */
/*                                                                      */
/* En sortie: Retourne un pointeur sur l'emprunt créé			        */
/*                                                                      */
/* Vars locales : tmp : ptr temporaire sur la nouvelle cellule créée    */
/* -------------------------------------------------------------------- */
emprunt_t *creerEmprunt(int num, int date_retour)
{
    emprunt_t * tmp = sddalloc(sizeof(emprunt_t));
    tmp->num = num;
    tmp->date_retour = date_retour;
    tmp->suiv = NULL;
    return tmp;
}

/* -------------------------------------------------------------------- */
/* recherchePrecedent   											    */
/* 																		*/
/* 			Recherche la cellule précédant la position d'insertion      */
/*          de la cellule recherchée passée en paramètres               */
/*                                                                      */
/* Principe: Parcours la liste tant que la place de l'élément           */
/*           n'est pas trouvée                                          */
/*                                                                      */
/* En entrée: La liste et la cellule recherchée						    */
/*                                                                      */
/* En sortie: Retourne un pointeur de pointeur sur la cellule précédente*/
/*                                                                      */
/* Vars locales : cour : ptr temporaire sur la cellule de parcours      */
/* -------------------------------------------------------------------- */
emprunt_t **recherchePrecedent(liste_emprunts *liste, int num)
{
    emprunt_t **cour = liste;
    while (*cour != NULL && (*cour)->num < num)
    {
        cour = &((*cour)->suiv);
    }
    return cour;
}

/* -------------------------------------------------------------------- */
/* inserer          Insère une cellule dans la liste de cellules        */
/*                                                                      */
/* Principe: Reforme le chaînage entre cellule et prec                  */
/*                                                                      */
/* En entrée: 															*/
/* 			  cellule : pointeur sur la cellule que l'on ajoute 		*/
/* 					  dans la liste                         			*/
/*            prec : pointeur de pointeur sur la cellule                */
/*                    d'insertion précédente                            */
/*                                                                      */
/* En sortie: Rien       												*/
/* -------------------------------------------------------------------- */
void inserer(emprunt_t *cellule, emprunt_t **prec)
{
    cellule->suiv = *prec;
    *prec = cellule;
}

/* -------------------------------------------------------------------- */
/* supprimer          Supprime un élément de la liste                   */
/*                                                                      */
/* Principe: Reforme le chaînage entre prec/suiv et libère la cellule   */
/*                                                                      */
/* En entrée: prec : cellule à supprimer                                */
/*                                                                      */
/* En sortie: 													        */
/* -------------------------------------------------------------------- */
void supprimer(emprunt_t **prec)
{
    if(*prec) {
        emprunt_t *tmp = *prec;
        (*prec) = (*prec)->suiv;
        free(tmp);
    }
}

/* -------------------------------------------------------------------- */
/* verifier          Effectue un contrôle des données et insère/suppr   */
/*                                                                      */
/* Principe: Vérifie qu'un emprunt n'existe pas déjà                    */
/*              et que le livre existe                                  */
/*                                                                      */
/* En entrée: emprunts : liste des emprunts                             */
/*            existant : résultat de la recherche dans la biblio        */
/*            nouvel_emprunt : nouvelle cellule d'emprunt               */
/*            insertion : booléen pour insertion ou suppression         */
/*                                                                      */
/* En sortie: code de retour de l'exécution								*/
/*                                                                      */
/* Vars locales : emprunt : ptr temporaire sur le nouvel emprunt créé   */
/*                code : code de retour d'exécution                     */
/* -------------------------------------------------------------------- */
int verifier(liste_emprunts * emprunts, livre_t * existant, int num, int date_retour, int insertion)
{
    emprunt_t * emprunt;
    int         code = EXIT_SUCCESS;

    if (existant != NULL)
    {
        if (insertion)
        {
            if (existant->est_emprunte == 0)
            {
                existant->est_emprunte = 1;
                emprunt = creerEmprunt(num, date_retour);
                inserer(emprunt, recherchePrecedent(emprunts, num));
            }
            else
            {
                fprintf(stderr, "Livre déjà emprunté\n");
                code = EXIT_FAILURE;
            }
        }
        else
        {
            existant->est_emprunte = 0;
            supprimer(recherchePrecedent(emprunts, num));
        }
    }
    else
    {
        fprintf(stderr, "Livre inexistant\n");
        code = EXIT_FAILURE;
    }

    return code;
}

/* -------------------------------------------------------------------- */
/* chargementEmprunts Lis et ajoute les données du fichier dans la liste*/
/*                                                                      */
/* Principe: Détermine si c'est une insertion ou non                    */
/*           puis lis le fichier indiqué en insérant ou supprimant      */
/*           les cellules créée à partir du fichier                     */
/*                                                                      */
/* En entrée: liste : liste de cellule									*/
/* 			  fichier : nom du fichier à lire                           */
/* 			  insertion : booléen pour insertion/suppression            */
/*                                                                      */
/* En sortie: Code de retour de la fonction       						*/
/*                                                                      */
/* Vars locales : flot : descripteur de fichier pour la lecture         */
/*                code : code de retour d'exécution                     */
/*                num : variable de stockage pendant la lecture         */
/*                date_retour : variable de stockage pendant la lecture */
/*                fichier : variable contenant le nom du fichier à lire */
/* -------------------------------------------------------------------- */
int chargementEmprunts(biblio_t biblio, liste_emprunts * emprunts, int insertion)
{
    FILE      * flot;
    int         code, num, date_retour;
    char        fichier[255];

    if (__DEBUG__)
    {
        strcpy(fichier, insertion ? "emprunts_ajout.txt" : "emprunts_suppr.txt");
    }
    else {
        printf("Indiquez le fichier :\t");
        scanf("%s", fichier);
    }
    
    flot = fopen(fichier, "r");

    if (flot == NULL)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier.\n");
        code = EXIT_FAILURE;
    }
    else
    {
        fscanf(flot, "%d\t%d", &num, &date_retour);
        while (!feof(flot))
        {
            verifier(emprunts, rechercheLivre(biblio, num), num, date_retour, insertion);
            fscanf(flot, "%d\t%d", &num, &date_retour);
        }

        code = EXIT_SUCCESS;
        fclose(flot);
    }

    return code;
}

/* -------------------------------------------------------------------- */
/* sauvegarder      Sauvegarde la liste de cellule dans un fichier      */
/*                                                                      */
/* Principe: Sauvegarde cellule par cellule en écrivant chaque ligne    */
/*           dans le fichier                                            */
/*                                                                      */
/* En entrée: liste liste de cellule à sauvegarder dans le fichier      */
/*                                                                      */
/* En sortie: Code de retour de la fonction       						*/
/*                                                                      */
/* Vars locales : tmp : ptr temporaire pour le parcours                 */
/*                flot : descripteur de fichier pour la lecture         */
/*                code : code de retour d'exécution                     */
/* -------------------------------------------------------------------- */
int sauvegarder(liste_emprunts liste){
    emprunt_t *tmp = liste;
    FILE *flot = fopen("emprunts.txt", "w");
    int code;

    if(flot == NULL){
        fprintf(stderr, "Impossible d'ouvrir le fichier.");
        code = EXIT_FAILURE;
    }
    else {
        while(tmp != NULL){
            fprintf(flot, "%d\t%d\n", tmp->num, tmp->date_retour);
            tmp = tmp->suiv;
        }
        code = EXIT_SUCCESS;
    }

    fclose(flot);

    return code;
}



/* -------------------------------------------------------------------- */
/* afficherEmprunts          Affiche les emprunts                       */
/*                                                                      */
/* Principe: Parcourt la liste jusqu'à la fin et l'affiche              */
/*                                                                      */
/* En entrée: emprunts : liste des emprunts à afficher                  */
/*            flux : flux de sortie                                     */
/*                                                                      */
/* En sortie: 													        */
/*                                                                      */
/* Vars locales : tmp : ptr temporaire pour le parcours                 */
/* -------------------------------------------------------------------- */
void afficherEmprunts(liste_emprunts emprunts, FILE * stream)
{
    emprunt_t *tmp = emprunts;
    while (tmp != NULL)
    {
        fprintf(stream, "%d\t%d\n", tmp->num, tmp->date_retour);
        tmp = tmp->suiv;
    }
}

/* -------------------------------------------------------------------- */
/* libererEmprunts   Libère la mémoire allouée pour les emprunts        */
/*                                                                      */
/* Principe: Parcourt la liste tout en libérant chaque cellule          */
/*                                                                      */
/* En entrée: emprunts : liste des emprunts                             */
/*                                                                      */
/* En sortie: 													        */
/*                                                                      */
/* Vars locales : cour : ptr temporaire pour le parcours                */
/*                tmp : ptr temporaire pour la libération de mémoire    */
/* -------------------------------------------------------------------- */
void libererEmprunts(liste_emprunts emprunts)
{
    emprunt_t *cour = emprunts, *tmp;
    while (cour != NULL)
    {
        tmp = cour;
        cour = cour->suiv;
        free(tmp);
    }
}

/* -------------------------------------------------------------------- */
/* afficherEmpruntsParDate          Affiche les emprunts                */
/*                                                                      */
/* Principe: Parcourt la liste jusqu'à la fin et affiche si la          */
/*           la condition est valide                                    */
/*                                                                      */
/* En entrée: emprunts : liste des emprunts à afficher                  */
/*                                                                      */
/* En sortie: 													        */
/*                                                                      */
/* Vars locales : tmp : ptr temporaire pour le parcours                 */
/* -------------------------------------------------------------------- */
void afficherEmpruntsParDate(liste_emprunts emprunts, FILE * flux)
{
    emprunt_t *tmp = emprunts;
    int date = 20202903;

    if (!__DEBUG__)
    {
        printf("Indiquer la date : ");
        scanf("%d", &date);
    }

    while (tmp != NULL)
    {
        if(tmp->date_retour < date) {
            fprintf(flux, "%d\t%d\n", tmp->num, tmp->date_retour);
        }
        tmp = tmp->suiv;
    }
}
