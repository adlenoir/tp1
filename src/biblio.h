#ifndef BIBLIO_H
#define BIBLIO_H

/* ------------------------------------ */
/* 			Structure de liste   		*/
/* ------------------------------------ */
typedef struct livre {
    int     num;
    char    titre[11];
    int     est_emprunte;
    struct  livre* suiv;
} livre_t, *catalogue_t;

/* ------------------------------------ */
/* 			Structure de liste   		*/
/* ------------------------------------ */
typedef struct categ {
    char            nom[4];
    catalogue_t     liste;
    struct categ*   suiv;
} categ_t, *biblio_t;

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */
biblio_t    creerBiblio();
livre_t   * rechercheLivre(biblio_t, int);
int         chargementBiblio(biblio_t *, char *);
void        afficherBiblio(biblio_t, FILE *);
void        libererBiblio(biblio_t);

#endif