#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include "biblio.h"
#include "utils.h"

/* -------------------------------------------------------------------- */
/* creerBiblio          		Créer une biblio vide 	                */
/*                                                                      */
/* principe: Sert de base en cas de structures de liste plus complexes  */
/*                                                                      */
/* En entrée:								                            */
/*                                                                      */
/* En sortie: Un pointeur pointant vers NULL					        */
/* -------------------------------------------------------------------- */
biblio_t creerBiblio()
{
    return NULL;
}

/* -------------------------------------------------------------------- */
/* recherche   											                */
/* 																		*/
/* 			Recherche une cellule                                       */
/*                                                                      */
/* principe: Parcours la liste tant que la place de l'élément           */
/*           n'est pas trouvée                                          */
/*                                                                      */
/* En entrée: La liste et le critère de recherche						*/
/*                                                                      */
/* En sortie: Retourne un pointeur de pointeur sur la cellule précédente*/
/*                                                                      */
/* Vars locales : categ_tmp : ptr temporaire pour le parcours           */
/*                livre_cour : ptr temporaire pour le parcours          */
/*                estTrouve : booléen indiquant si un élmt a été trouvé */
/* -------------------------------------------------------------------- */
livre_t *rechercheLivre(biblio_t biblio, int num)
{
    categ_t *categ_tmp = biblio;
    livre_t *livre_cour = NULL;
    int estTrouve = 0;

    while (!estTrouve && categ_tmp != NULL)
    {
        livre_cour = categ_tmp->liste;
        while (!estTrouve && livre_cour != NULL && livre_cour->num <= num)
        {
            if (livre_cour->num == num)
            {
                estTrouve = 1;
            }
            else {
                livre_cour = livre_cour->suiv;
            }
        }
        categ_tmp = categ_tmp->suiv;
    }

    return livre_cour;
}

/* -------------------------------------------------------------------- */
/* chargementBiblio Lis et ajoute les données du fichier dans la liste  */
/*                                                                      */
/* principe: Lis le nb de livres sur la 1ère ligne et insère en boucle  */
/*           les catégories en tête puis les livres en queue            */
/*                                                                      */
/* En entrée: biblio : liste de cellule									*/
/* 			  fichier : nom du fichier à lire                           */
/*                                                                      */
/* En sortie: Code de retour de la fonction       						*/
/*                                                                      */
/* Vars locales : flot : descripteur de fichier pour la lecture         */
/*                i : variable d'itération dans le for                  */
/*                code : code de retour d'exécution                     */
/*                num_livre : variable de stockage pendant la lecture   */
/*                nb_livres : variable de stockage pendant la lecture   */
/*                titre : variable de stockage pendant la lecture       */
/*                nom : variable de stockage pendant la lecture         */
/*                livre_tmp : ptr temporaire pour le parcours           */
/*                livre_prec : ptr temporaire sur le maillon précédent  */
/* -------------------------------------------------------------------- */
int chargementBiblio(biblio_t * biblio, char * fichier)
{
    FILE      * flot = fopen(fichier, "r");
    int         i, code, num_livre, nb_livres;
    char        titre [20], nom[3];
    categ_t   * categ_tmp;
    livre_t   * livre_tmp, ** livre_prec = NULL;

    if(flot == NULL){
        fprintf(stderr, "Impossible d'ouvrir le fichier.\n");
        code = EXIT_FAILURE;
    }
    else {
        fscanf(flot, "%s\t%d\n", nom, &nb_livres);
        while(!feof(flot)){

            categ_tmp = sddalloc(sizeof(categ_t));
            strncpy(categ_tmp->nom, nom, 4);
            categ_tmp->liste = NULL;
            livre_prec = &(categ_tmp->liste);

            for(i = 0; i < nb_livres; ++i) {
                fscanf(flot, "%d\t", &num_livre); 
                fgets(titre, 20, flot);

                titre[strcspn(titre, "\r\n")] = 0;

                livre_tmp = sddalloc(sizeof(livre_t));
                strcpy(livre_tmp->titre, titre);
                livre_tmp->num = num_livre;
                livre_tmp->suiv = NULL;
                livre_tmp->est_emprunte = 0;

                *livre_prec = livre_tmp;
                livre_prec = &(livre_tmp->suiv);
            }
            
            categ_tmp->suiv = *biblio; 
            (*biblio) = categ_tmp;

            fscanf(flot, "%s\t%d\n", nom, &nb_livres);
        }

        code = EXIT_SUCCESS;
        fclose(flot);
    }

    return code;
}

/* -------------------------------------------------------------------- */
/* afficherBiblio          Affiche la liste                             */
/*                                                                      */
/* principe: Parcourt la liste jusqu'à NULL et affiche                  */
/*                                                                      */
/* En entrée: biblio : liste de cellules à afficher                     */
/*            flux : flux de sortie                                     */
/*                                                                      */
/* En sortie: 													        */
/*                                                                      */
/* Vars locales : categ_tmp : ptr temporaire pour le parcours           */
/*                livre_tmp : ptr temporaire pour le parcours           */
/* -------------------------------------------------------------------- */
void afficherBiblio(biblio_t biblio, FILE * flux)
{
    categ_t * categ_tmp = biblio;
    livre_t * livre_tmp;

    while(categ_tmp != NULL){
        fprintf(flux, "%s\n", categ_tmp->nom);
        livre_tmp = categ_tmp->liste;

        while(livre_tmp != NULL) {
            fprintf(flux, "%d\t%s\n", livre_tmp->num, livre_tmp->titre);
            livre_tmp = livre_tmp->suiv;
        }

        categ_tmp = categ_tmp->suiv;
    }
}

/* -------------------------------------------------------------------- */
/* libererBiblio          Libère la mémoire allouée pour la biblio      */
/*                                                                      */
/* principe: Parcourt les listes jusqu'à NULL en libérant la mémoire    */
/*                                                                      */
/* En entrée: biblio : liste de cellules                                */
/*                                                                      */
/* En sortie: 													        */
/*                                                                      */
/* Vars locales : categ_cour : ptr courant pour le parcours             */
/*                categ_tmp : ptr temporaire pour la suppression        */
/*                livre_cour : ptr courant pour la suppression          */
/*                livre_tmp : ptr temporaire pour la suppression        */
/* -------------------------------------------------------------------- */
void libererBiblio(biblio_t biblio)
{
    categ_t * categ_cour = biblio, *categ_tmp;
    livre_t * livre_cour, *livre_tmp;

	while(categ_cour != NULL) {
        categ_tmp = categ_cour;
        livre_cour = categ_tmp->liste;

        while(livre_cour != NULL) {
            livre_tmp = livre_cour;
            livre_cour = livre_cour->suiv;
            free(livre_tmp);
        }

        categ_cour = categ_cour->suiv;
        free(categ_tmp);
    }
}