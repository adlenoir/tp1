#ifndef EMPRUNTS_H
#define EMPRUNTS_H

#include "biblio.h"

/* ------------------------------------ */
/* 			Structure de liste   		*/
/* ------------------------------------ */
typedef struct emprunt
{
    int             num;
    int             date_retour;
    struct emprunt  *suiv;
} emprunt_t, *liste_emprunts;

/* -------------------------------- */
/* 			Prototypes   			*/
/* -------------------------------- */
liste_emprunts  creerEmprunts();
emprunt_t       *creerEmprunt(int, int);
int             verifier(liste_emprunts *, livre_t *, int, int, int);
emprunt_t       **recherchePrecedent(liste_emprunts *, int);
void            inserer(emprunt_t *, emprunt_t **);
void            supprimer(emprunt_t **prec);
int             chargementEmprunts(biblio_t, liste_emprunts *, int);
int             sauvegarder(liste_emprunts);
void            afficherEmprunts(liste_emprunts, FILE *);
void            libererEmprunts(liste_emprunts);
void            afficherEmpruntsParDate(liste_emprunts, FILE *);

#endif