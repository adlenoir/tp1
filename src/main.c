#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "biblio.h"
#include "emprunts.h"
#include "teZZt.h"

/* -------------------------------------------------------------------- */
/* TESTS        DÉBUT Jeux de tests                                       */
/* -------------------------------------------------------------------- */
BEGIN_TEST_GROUP(tp1)

TEST(BIBLIO, "Tests bibliothèque") {
    char
        buffer[1024] = "",
        resultatGeneral[] = "BD\n21\ttintin\n23\ttiteuf\nPOL\n11\tle tueur\n15\tla victime\n17\tle truand\n",
        resultatVide[] = "",
        resultatEmpruntsGeneral[] = "11\t20202803\n17\t20202803\n23\t20202803\n",
        resultatEmpruntsSuppr[] = "11\t20202803\n",
        resultatEmpruntsParDate[] = "11\t20202803\n";
    FILE * file;
    biblio_t biblio;
    liste_emprunts emprunts;

    /********* Cas de la bibliothèque *********/
        /*Cas général*/
        biblio = creerBiblio();
        file = fmemopen(buffer, 1024, "w");
        CHECK(biblio == NULL);
        CHECK(chargementBiblio(&biblio, "donnee.txt") == EXIT_SUCCESS);
        afficherBiblio(biblio, file);
        fclose(file);
        CHECK(!strcmp(buffer, resultatGeneral));
        libererBiblio(biblio);
        strcpy(buffer, "");
        /*Cas général*/

        /*Cas Fichier inexistant*/
        biblio = creerBiblio();
        CHECK(chargementBiblio(&biblio, "inexistant.txt") == EXIT_FAILURE);
        libererBiblio(biblio);
        /*Cas Fichier inexistant*/

        /*Cas Fichier vide*/
        biblio = creerBiblio();
        file = fopen("vide.txt", "w");
        fclose(file);
        CHECK(chargementBiblio(&biblio, "vide.txt") == EXIT_SUCCESS);
        file = fmemopen(buffer, 1024, "w");
        afficherBiblio(biblio, file);
        fclose(file);
        CHECK(!strcmp(buffer, resultatVide));
        libererBiblio(biblio);
        strcpy(buffer, "");
        remove("vide.txt");
        /*Cas Fichier vide*/
    /********* Cas de la bibliothèque *********/

    /********* Cas des emprunts *********/
    biblio = creerBiblio();
    chargementBiblio(&biblio, "donnee.txt");

        /*Cas général*/
        emprunts = creerEmprunts();
        file = fmemopen(buffer, 1024, "w");
        CHECK(emprunts == NULL);
        CHECK(chargementEmprunts(biblio, &emprunts, 1) == EXIT_SUCCESS);
        afficherEmprunts(emprunts, file);
        fclose(file);
        CHECK(!strcmp(buffer, resultatEmpruntsGeneral));
        strcpy(buffer, "");
        /*Cas général*/

        /*Cas livre inexistant*/
        CHECK(verifier(&emprunts, rechercheLivre(biblio, 42), 42, 20202803, 1) == EXIT_FAILURE);
        /*Cas livre inexistant*/

        /*Cas livre déjà emprunté*/
        CHECK(verifier(&emprunts, rechercheLivre(biblio, 23), 23, 20202803, 1) == EXIT_FAILURE);
        /*Cas livre déjà emprunté*/

        /*Suppression des emprunts*/
        CHECK(chargementEmprunts(biblio, &emprunts, 0) == EXIT_SUCCESS);
        file = fmemopen(buffer, 1024, "w");
        afficherEmprunts(emprunts, file);
        fclose(file);
        CHECK(!strcmp(buffer, resultatEmpruntsSuppr));
        strcpy(buffer, "");
        /*Suppression des emprunts*/

        /*Affichage des emprunts inférieur à une date donnée*/
        file = fmemopen(buffer, 1024, "w");
        afficherEmpruntsParDate(emprunts, file);
        fclose(file);
        CHECK(!strcmp(buffer, resultatEmpruntsParDate));
        strcpy(buffer, "");
        /*Affichage des emprunts inférieur à une date donnée*/

        libererEmprunts(emprunts);
        libererBiblio(biblio);
        /********* Cas des emprunts *********/
}

END_TEST_GROUP(tp1)
/* -------------------------------------------------------------------- */
/* TESTS        FIN Jeux de tests                                       */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/* main        Programme principal                                      */
/*                                                                      */
/* principe: Créer le menu et lance les tests                           */
/*                                                                      */
/* En entrée: argc : taille du tableau argv                             */
/*            argv : tableau des arguments du programme                 */
/*                                                                      */
/* En sortie: code de retour de la fonction pour le processus           */
/* -------------------------------------------------------------------- */
int main(int argc, char* argv[])
{
    biblio_t biblio = creerBiblio();
    liste_emprunts emprunts = creerEmprunts();
    int choix = -1;

    if(argc > 1){
        puts("Chargement ...");
        if(chargementBiblio(&biblio, argv[1]) == EXIT_SUCCESS){
            puts("Affichage de la biblio : ");
            afficherBiblio(biblio, stdout);

            while(choix != 5) {

                puts("--------------------- MENU --------------------------");
                puts("1) Ajouter les emprunts enregistrés");
                puts("2) Supprimer les emprunts enregistrés");
                puts("3) Afficher les emprunts par date");
                puts("4) Lancer les tests");
                puts("5) Quitter le programme");
                puts("-----------------------------------------------------");

                printf("Choix ?\t");
                scanf("%d", &choix);

                switch(choix) {
                    case 1:
                        puts("Chargement des emprunts...");
                        if (chargementEmprunts(biblio, &emprunts, 1) == EXIT_SUCCESS)
                        {
                            afficherEmprunts(emprunts, stdout);
                        }
                        break;
                    case 2:
                        puts("Suppression des emprunts...");
                        if (chargementEmprunts(biblio, &emprunts, 0) == EXIT_SUCCESS)
                        {
                            afficherEmprunts(emprunts, stdout);
                        }
                        break;
                    case 3:
                        puts("Affichage des emprunts par date...");
                        afficherEmpruntsParDate(emprunts, stdout);
                        break;
                    case 4:
                        RUN_TEST_GROUP(tp1); 
                    break;
                }
            }
        }
    }
    
    sauvegarder(emprunts);
    libererBiblio(biblio);
    libererEmprunts(emprunts);
    
    return EXIT_SUCCESS;
}